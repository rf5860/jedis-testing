package com.peopleinsite;

import redis.clients.jedis.Jedis;

public class HelloRedis {
    public static void main(String[] args) {
        for (String host : new String[] { "127.0.0.1", "localhost"}) {
            System.out.println("Testing with host " + host);
            Jedis jedis = new Jedis(host);
            jedis.set("test", "A");
            System.out.println("Getting value: " + jedis.get("test"));
        }
    }
}
